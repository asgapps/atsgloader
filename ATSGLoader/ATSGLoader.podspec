#
#  Be sure to run `pod spec lint ATSGLoader.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

s.name         = "ATSGLoader"
s.version      = "0.0.1"
s.summary      = "ATSG Loader is a simple activity indicator to use in all application."
s.description  = <<-DESC
                    ATSG Loader is a simple activity indicator to use in all application. Su uso es muy fácil tiene dos métodos: uno para mostrar el indicador y otro para ocultarlo.
                DESC

s.homepage     = "https://bitbucket.org/asgapps/atsgloader/overview"
s.authors      = { "Antonio Serrano" => "serranogomezantonio@gmail.com" }
s.license      = { :type => 'Apache License, Version 2.0', :text => <<-LICENSE
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
LICENSE
}
s.source       = { :git => "https://asgapps@bitbucket.org/asgapps/atsgloader.git", :tag => s.version }
s.source_files  = "ATSGLoader/*.swift"
s.ios.deployment_target = "8.0"


end
