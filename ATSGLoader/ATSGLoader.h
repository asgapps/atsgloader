//
//  ATSGLoader.h
//  ATSGLoader
//
//  Created by Antonio Serrano on 29/11/16.
//  Copyright © 2016 ViewNext. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ATSGLoader.
FOUNDATION_EXPORT double ATSGLoaderVersionNumber;

//! Project version string for ATSGLoader.
FOUNDATION_EXPORT const unsigned char ATSGLoaderVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ATSGLoader/PublicHeader.h>


