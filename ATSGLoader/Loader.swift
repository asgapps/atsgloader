//
//  Loader.swift
//  ATSGLoader
//
//  Created by Antonio Serrano on 29/11/16.
//  Copyright © 2016 ASGAPPS. All rights reserved.
//

import UIKit

public class BProgressHUD: NSObject {
    
    static var backView:UIView?
    
    class func showLoadingViewWithMessage(msg: String?){
        
        if backView != nil {
            
            dismiss()
            
        }
        
        backView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        backView!.backgroundColor = UIColor.clear
                
        let messageBackView:UIView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        messageBackView.layer.cornerRadius = 8
        messageBackView.layer.masksToBounds = true
        messageBackView.backgroundColor = UIColor.black
        messageBackView.center = CGPoint(x:UIScreen.main.bounds.width / 2, y:UIScreen.main.bounds.height / 2)
        backView!.addSubview(messageBackView)
        
        let indicator:UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        indicator.frame = CGRect(x:30, y:16, width:40, height:40)
        indicator.startAnimating()
        indicator.backgroundColor=UIColor.clear
        messageBackView.addSubview(indicator)
        
        let messageLabel:UILabel = UILabel(frame: CGRect(x:0, y:56, width:messageBackView.frame.size.width, height:32))
        messageLabel.font = UIFont(name: "HelveticaNeue", size: 13)
        messageLabel.textColor = UIColor.white
        messageLabel.textAlignment = NSTextAlignment.center
        messageLabel.text = msg
        messageBackView.addSubview(messageLabel)
        
        UIApplication.shared.keyWindow!.addSubview(backView!)
    }
    
    class func dismiss(){
        backView?.removeFromSuperview()
    }
}
